#region License and Terms
/*
The MIT License (MIT)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#endregion

#region Using declarations
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
#endregion

namespace NinjaTrader.Indicator {

	[Description("Vitalij's volume indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitVolume : Indicator {

		double ask = 0;
		double bid = 0;
		bool addHistoricalVolume = true;

		protected override void Initialize() {
			CalculateOnBarClose = false;
			Overlay = false;
			PlotsConfigurable = true;
			LinesConfigurable = true;

			Name = "Vitalij's Volume Indicator";

			Add(new Line(Color.FromArgb(64, 64, 64), 0, "zero line"));
			Add(new Plot(new Pen(Color.FromArgb(64, 64, 64), 2), PlotStyle.Bar, "03. unknown ask"));
			Add(new Plot(new Pen(Color.FromArgb(64, 64, 64), 2), PlotStyle.Bar, "05. unknown bid"));
			Add(new Plot(new Pen(Color.Green, 2), PlotStyle.Bar, "02. at ask"));
			Add(new Plot(new Pen(Color.Red, 2), PlotStyle.Bar, "06. at bid"));
			Add(new Plot(new Pen(Color.FromArgb(0, 255, 72), 2), PlotStyle.Bar, "01. above ask"));
			Add(new Plot(new Pen(Color.Gold, 2), PlotStyle.Bar, "07. below bid"));
			Add(new Plot(new Pen(Color.Silver, 1), PlotStyle.Line, "04. balance"));
		}

		protected override void OnBarUpdate() {
			if (Historical) {
				var half = Volumes[0][0] / 2;
				UnknownAsk.AddAndSum(half);
				UnknownBid.AddAndSum(-half);
			} else if (addHistoricalVolume) {
				addHistoricalVolume = false;
				AboveAsk.AddAndSum(0);
				BelowBid.AddAndSum(0);
				var half = (Volumes[0][0] - AboveAsk[0] + BelowBid[0]) / 2;
				AboveAsk.AddAndSum(half);
				BelowBid.AddAndSum(-half);
				AtAsk.AddAndSum(half);
				AtBid.AddAndSum(-half);
				if (CalculateOnBarClose) {
					UnknownAsk.AddAndSum(half);
					UnknownBid.AddAndSum(-half);
				}
			}
		}

		protected override void OnMarketData(MarketDataEventArgs args) {
			if (args.MarketDataType == MarketDataType.Last) {
				int bar = CalculateOnBarClose ? -1 : 0;
				if (ask > bid && args.Price > ask) {
					AboveAsk.AddAndSum(bar, args.Volume);
					BelowBid.AddAndSum(bar, 0);
				} else if (ask > bid && args.Price < bid) {
					AboveAsk.AddAndSum(bar, 0);
					BelowBid.AddAndSum(bar, -args.Volume);
				} else if (ask > bid && args.Price == ask) {
					AboveAsk.AddAndSum(bar, args.Volume);
					BelowBid.AddAndSum(bar, 0);
					AtAsk.AddAndSum(bar, args.Volume);
				} else if (ask > bid && args.Price == bid) {
					AboveAsk.AddAndSum(bar, 0);
					BelowBid.AddAndSum(bar, -args.Volume);
					AtBid.AddAndSum(bar, -args.Volume);
				} else {
					var half = args.Volume / 2;
					AboveAsk.AddAndSum(bar, half);
					BelowBid.AddAndSum(bar, -half);
					AtAsk.AddAndSum(bar, half);
					AtBid.AddAndSum(bar, -half);
					UnknownAsk.AddAndSum(bar, half);
					UnknownBid.AddAndSum(bar, -half);
				}
				Difference.Set(bar, AboveAsk[bar] + BelowBid[bar]);
			} else if (args.MarketDataType == MarketDataType.Ask) {
				ask = args.Price;
			} else if (args.MarketDataType == MarketDataType.Bid) {
				bid = args.Price;
			}
		}

		#region Properties
		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries Difference {
			get { return Values[6]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries AboveAsk {
			get { return Values[4]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries AtAsk {
			get { return Values[2]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries UnknownAsk {
			get { return Values[0]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries UnknownBid {
			get { return Values[1]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries AtBid {
			get { return Values[3]; }
		}

		[Browsable(false)]
		[XmlIgnore()]
		public DataSeries BelowBid {
			get { return Values[5]; }
		}
		#endregion

	}
}
