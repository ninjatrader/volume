#region License and Terms
/*
The MIT License (MIT)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#endregion

#region Using declarations

using NinjaTrader.Data;

#endregion Using declarations

namespace NinjaTrader.Indicator {
	public static class VitCommon {

		public static void AddAndSum(this DataSeries ds, double value) {
			ds.AddAndSum(0, value);
		}

		public static void AddAndSum(this DataSeries ds, int barsAgo, double value) {
			if (ds.ContainsValue(barsAgo)) {
				ds[barsAgo] += value;
			} else {
				ds.Set(barsAgo, value);				
			}
		}
		
	}
}
