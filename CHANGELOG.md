Changelog
=========

1.0.1
-----

* fixed missing volume on startup
* fixed CalculateOnBarClose mode
* new colors


1.0.0
-----

* initial release
